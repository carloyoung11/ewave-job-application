//Get the button:
mybutton = document.getElementById("myBtn");
var rootElement = document.documentElement;

// When the user scrolls down 50% from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight;
  if (rootElement.scrollTop / scrollTotal > 0.5)  {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


let slider = tns({
    container: ".my-slider",
    "slideBy": "1",
    "speed" : 400,
    "nav" : false,
    autoplay:true,
    controls:false,
    autoplayButtonOutput: false,
    responsive: {
        1600: {
            items: 3,
            gutter : 20
        },
        1024: {
            items: 3,
            gutter : 20
        },
        768: {
            items: 3,
            gutter : 20
        },
        480: {
            items: 1
        }
    }
})


let generate_btn = document.querySelector(".generate_btn");

generate_btn.addEventListener("click", fetchPics)

function fetchPics() {
    let catsImgDiv = document.querySelector(".catsImgDiv")
    catsImgDiv.innerHTML = ''

    fetch('https://api.thecatapi.com/v1/images/search')
    .then(response => response.json())
    .then((data) => {
        let catsImgUrl = data[0].url

        let catsImgEl = document.createElement("img")
        catsImgEl.setAttribute('src', `${catsImgUrl}`)

        let catsImgDiv = document.querySelector(".catsImgDiv")
        catsImgDiv.appendChild(catsImgEl)
    })
    .catch(err => console.log(err))
}
